/**
* This script is the non-minified client script.
* This script will fetch data from route and initialise layout component
*/

import React from "react";
import ReactDOM from "react-dom";
import {Router, Route, IndexRoute, hashHistory} from "react-router";
import 'isomorphic-fetch';

import Layout from "../views/layout/layout.js"

const app = document.getElementById("app");

fetch(window.location.origin+"/data")
	.then(function(result) {
		return result.json();
	}).then(function(json) {
		ReactDOM.render(
			<Router history={hashHistory}>
				<Route path="/" component={Layout} data={json}>
				</Route>
			</Router>,
		app);
	}).catch(function(err) {
		console.log("err", err)
	});




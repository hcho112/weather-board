/**
* Main server side express
* This script vaguely defines routing of the application
* Port number is defined in config
*
* To do: create proper routing directory, decouple access.
*/

var express = require('express');
var app = express();
var config = require('./config');
var bodyParser = require('body-parser');

app.set('port', process.env.port || config.port);
app.engine('html', require('ejs').renderFile);
app.use(express.static(__dirname));


app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// Configure the database initial connection
var mongoose = require('mongoose');
mongoose.connect(config.db_url);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  console.log('MongoDB connection success');
});

var locationModel = require('./models/location.js');

//Default 
app.get('/', function (req, res) {
  res.render('index.html');
});


//Requeting data
app.get('/data', function (req, res) {
  	locationModel.find({}).sort({country:"asc"}).exec(function(err, locations) {
		res.json(locations);
	});
});


//Delete route
app.post('/delete', function(req, res) {
  locationModel.find(req.body).remove({}, function(err, deletedItem){
  	//Fail
  	if(err) {
  		res.send("Fail");
  	//Success
  	} else {
  		res.send("Success");
  	}
  });
});

//Update route
app.post('/update', function(req, res) {
  locationModel.update({_id:req.body._id}, req.body, function(err, updatedItem){
    //Fail
    if(err) {
      res.send("Fail");
    //Success
    } else {
      res.send("Success");
    }
  });
});

app.post('/create', function(req, res) {
  locationModel.create(req.body, function(err, updatedItem){
    //Fail
    if(err) {
      res.send("Fail");
    //Success
    } else {
      res.send("Success");
    }
  })
});


var server = app.listen(app.get('port'), function () {
	console.log("Application is serving at localhost:"+config.port);
});




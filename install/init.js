/**
* This script is design to feed location data (capital, country) into MongoDB prior to application start	
* It is assumed that MongoDB is up and accessable and timezone fetching service is up and available (For detail, please review ../config/index.js)
*/


// root
global.__base = __dirname + '/../';

// Dependencies
var config = require('../config');
require('isomorphic-fetch');

// Connect to MongoDB
// Configure the database initial connection
var mongoose = require('mongoose');
mongoose.connect(config.db_url);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  console.log('MongoDB connection ready');
});

var insertedCount = 0;

//Load Model
var locationModel = require(__base + 'models/location.js');

const insertOneLocation = function(location) {
	return new Promise(function(resolve, reject) {
		// Get timezone of the location
		var capital = location.capital;
		var country = location.country;

		// Query MongoDB to check exists
		locationModel.findOne({capital:capital, country:country}, function(err, locationItem) {
			if(err) {
				console.log("MongoDB Error: " + err);
				// return false;
			}
			if(!locationItem) {
				console.log("No item found, create new location");
				
				fetch(config.tz_api+'&format=json&q='+capital)
					.then(function(result) {
						return result.json();
					}).then(function(json) { 
						
						var gmt_diff_hrs = 0;
						var gmt_diff_hrs_exist = false;
						if(json.data.error == undefined) {
							gmt_diff_hrs = json.data.time_zone[0].utcOffset;
							gmt_diff_hrs_exist = true;
						}

						locationModel.create(
							{
								country: country,
								capital: capital,
								gmt_diff_hrs: gmt_diff_hrs,
								gmt_diff_hrs_exist:gmt_diff_hrs_exist
								
							}, function(err, createdItem) {
								if(err) {
									console.log("MongoDB Error: " + err);
									reject(err);
								}
								if(createdItem) {
									console.log("Inserted "+ createdItem.country+"'s Data");
									insertedCount++;
									resolve();
								}
							}
						);
					}).catch(function(err) {
						reject(err);
					});
			} else {
				console.log(country + " already exist");
				resolve();
			}
		});
	});
}

// Main function to loop
var Converter = require("csvtojson").Converter;
var converter = new Converter({});
converter.fromFile("../public/data/country-list.csv",function(err,result){

	const promises = result.map(insertOneLocation);

	Promise.all(promises)
		.then(function() {
			// Disconnect from MongoDB
			console.log("Pre-Fetching complete, "+insertedCount+" records are inserted")
			mongoose.connection.close();
		})
		.catch(function(err) {
			console.log(err);
		})
});
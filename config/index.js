/**
* This script contains default key or any constant.
* tz_api is used as acquiring timezone as a service from https://developer.worldweatheronline.com/
* This service have maximum 500 request per day limit and trial ends 12 June 2016.
* To extend, please e-mail hj.danny.cho@gmail.com
*/


module.exports = {
	db_url: 'mongodb://127.0.0.1:27017/weatherboard',
	tz_api: "http://api.worldweatheronline.com/premium/v1/tz.ashx?key=a159eaf788064c3dacc42758161304",
	weather_api: "https://query.yahooapis.com/v1/public/yql?q=",
	timeout: 5000,
	port:9080
};
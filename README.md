# Weather Board
This application shows table of countries, their capital city, current weather and live clock of each.

## Installation
	(During the 'npm install', it may asks user to type password for sudo commands)
	git clone https://hcho112@bitbucket.org/hcho112/weather-board.git
	cd weather-board
	npm install
	npm start

## Condition & Limitation
1. Installed environment must be OS X.
2. User of OS X must have root permission. (Or simply type current's user's password for sudo commands)
(Guide link: https://support.apple.com/en-nz/HT204012)
3. localhost:9080 must be available.
4. Timezone API is currently using trial version that valid till 12th of June 2016.
5. Timezone API is limited to 500 request per day. 
(Some people not be able to retrieve timezone)

## Future work
1. Create notification feature to let user know when operation is happened.
2. Flexibility of environment to get installed. 
3. Create add record feature to let user dynamically add city and country.
4. Replace Timezone API with more robustness.

## License
ISC license

var debug = process.env.NODE_ENV !== "production";
var webpack = require('webpack');

module.exports = {
  context: __dirname,
  devtool: debug ? "inline-sourcemap" : null,
  entry: "./client/client.js",
  module: {
    loaders: [
      {
        test: /\.js?$/,
        exclude: /(node_module|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015']
        }

      }
    ]
  },
  output: {
    path: __dirname,
    filename: "client.min.js"
  },
  plugins: debug ? [] : [
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
  ],
};
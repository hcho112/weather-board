
echo "Install WeatherBoard Application"
mkdir log;
chmod 777 log;
brew install mongodb;
sudo mkdir /data;
sudo chmod 777 /data;
mkdir /data/db;
mongod --fork --logpath log/mongod.log;

echo "Initial data import start";
cd install;
node init.js;
echo "data import finish";

cd ..;
webpack;

echo "WeatherBoard Application is ready to run"

/**
 * Mongoose schema for a location (city and country) 
 */

var mongoose = require('mongoose');


//Unsure about wheather to assign unique ID or not.
var locationSchema = mongoose.Schema({

    capital: String,

    country: String,

    gmt_diff_hrs: Number,

    gmt_diff_hrs_exist: Boolean
});

//create unique key using captial and country 
locationSchema.index({capital: 1, country: 1}, {unique: true});

module.exports = mongoose.model('Location', locationSchema);
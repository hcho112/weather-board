/**
* This component is responsible for rendering body
*/

import React from "react";
import ReactDOM from "react-dom";
import CustomModal from "../component/custommodal.js";
import WeatherTable from "../component/weathertable.js";
import {Button} from "react-bootstrap";

export default class Body extends React.Component {
	
	createPanel() {
		let title = "Create New Entry";
		let ok_button = {};
		ok_button.label = "Create";
		ok_button.bsStyle = "primary";
		let targetDocument = {country:"", capital:"", gmt_diff_hrs:0};
		let change = window.dataChange;
		let info = {title, ok_button, targetDocument, change};
		ReactDOM.render(<CustomModal Info={info}/>, document.getElementById('panel'));
	}

	render() {
		
		const buttonStyle = {
			float:"right"
		}; 

		return (
			<div>
				<Button bsStyle="primary" style={buttonStyle} onClick={this.createPanel.bind(this)}>Create</ Button>
				<WeatherTable data={this.props.data} />
			</div>
		);
	}
}
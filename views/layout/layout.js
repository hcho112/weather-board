/**
* This layout component is showing header and body component
* 
* To do: may add footer and required information
*/

import React from "react";
import Header from "./header.js";
import Body from "./body.js";


export default class Layout extends React.Component {
	render() {
		const tableStyle = {
			margin: "0px 10px",
			width: "calc(100% - 20px)"
		};

		return (
			<div>
				<Header />
				<div style={tableStyle}>
					<Body data={this.props.routes[0].data} />
				</div>
			</div>
		)
	}
}
/**
* This component is responsible for rendering header.
* Currently using bootstrap 3.0v css
* 
* To do: possible to extend header menu here
*/


import React from "react";

export default class Header extends React.Component {
	render() {
		const iconStyle = {
			marginRight: "20px"
		};

		return (
			<nav className="navbar navbar-default" >
				<a className="navbar-brand" href="#">
					<i className="fa fa-table" aria-hidden="true" style={iconStyle}></i>
					Weather Board
				</a>
			</nav>
		);
	}
}
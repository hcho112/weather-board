/**
* This component is responsible for deleteing a document/row of a table
* It requires CustomModal component
*/

import React from "react";
import ReactDOM from "react-dom";
import config from "../../config";
import CustomModal from "./custommodal";

export default class DeleteRow extends React.Component {
	constructor(props) {
	    super(props);
		this.state = {};
	}

	createPanel() {
		let title = "Delete Row";
		let body = "Are you sure you want to delete '"+this.props.Document.country +"' records?";
		let ok_button = {};
		ok_button.label = "Delete";
		ok_button.bsStyle = "danger";
		let targetDocument = this.props.Document;
		let change = this.props.Change;
		let info = {title, body, ok_button, targetDocument, change};

		ReactDOM.render(<CustomModal Info={info}/>, document.getElementById('panel'));
	}

	render() {
		const buttonStyle = {
			float:"left"
		};

		return (
			<button type="button" className="close" aria-label="Close" style={buttonStyle} onClick={this.createPanel.bind(this)}>
				<span aria-hidden="true">&times;</span>
			</button>
		);	
	}
}
/**
* This component is responsible for building a row in table
* All custom column components should be imported to this component
*/

import React from "react";
import TimeColumn from "./timecolumn.js";
import WeatherColumn from "./weathercolumn.js";
import DeleteRow from "./deleterow.js";
import EditRow from "./editrow.js";

export default class CustomRow extends React.Component {
	render() {
		const singleDocument = {
			_id : this.props.Id,
			capital: this.props.Capital,
			country: this.props.Country,
			gmt_diff_hrs: this.props.Timezone
		}

		return (
			<tr>
				<td key={this.props.Id + "0"}>{this.props.Country}</td>
				<td key={this.props.Id + "1"}>{this.props.Capital}</td>
				<td key={this.props.Id + "2"}><WeatherColumn Capital={this.props.Capital} /></td>
				<td key={this.props.Id + "3"}><TimeColumn Timezone={this.props.Timezone} Enable={this.props.Enable}/></td>
				<td key={this.props.Id + "4"}><EditRow Change={this.props.Change} Document={singleDocument}/></td>
				<td key={this.props.Id + "5"}><DeleteRow Change={this.props.Change} Document={singleDocument}/></td>
			</tr>
		);
	}
}
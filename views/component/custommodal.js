/**
* This component is responsible for rendering modal 
* It requires arguments of title, body, ok_button ({bsStyle, label}), targetDocument and Change (updating table)
* Currently this component is design to serve two mode, delete row and update row
* If this.props.Info.body is set, it's delete mode, else it's update mode
*/

import React from "react";
import {Modal, Button, Input} from "react-bootstrap";
import ReactDOM from "react-dom";

const CustomModal = React.createClass({
  getInitialState() {
  	if(!this.state) {
  		this.state = {};
  	}
  	this.open = true;
    this.state.showModal = true;
    this.state.warning="";
    this.state.timer = "";

    //risky approach, make sure name of button corespond to type of operation
    this.state.mode = this.props.Info.ok_button.label.toLowerCase(); 
    
    return {  showModal: true, 
              mode:this.state.mode, 
              targetDocument:this.props.Info.targetDocument,
              change:this.props.Info.change};
  },

  //callback to handle update and delete
  callback() {
    fetch(window.location.origin+'/'+this.state.mode, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(this.state.targetDocument)
      }).then(function(response) {
        return response.text();
      }).then(function(response) {
        if(response === "Success") {
          this.state.change(this.state.targetDocument, this.state.mode);
        }
        this.close();
      }.bind(this));
  },

  close() {
    this.setState({ showModal: false });
    ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this).parentNode);
  },

  open() {
    this.setState({ showModal: true });
  },

  render() {

    //handle onChange per input
    const updateCountry=(e)=>{
      this.state.targetDocument.country = e.target.value;
      this.setState({targetDocument: this.state.targetDocument});
    };

    const updateCapital=(e)=> {
      this.state.targetDocument.capital = e.target.value;
      this.setState({targetDocument: this.state.targetDocument});
    };

    const updateTimezone=(e)=> {
      clearTimeout(this.state.timer);
      //numeric number validation
      if(!isNaN(e.target.value) || e.target.value === "") {
        this.state.targetDocument.gmt_diff_hrs = e.target.value;
        this.setState({targetDocument: this.state.targetDocument});
        this.setState({warning:""});
      } else {
        //if not a number, display error theme and not update
        this.setState({warning:"error"});
        this.state.timer = setTimeout(()=>{
          this.setState({warning:""});
        },1000);
      }
      
    };

    return (
      <div>
        <Modal show={this.state.showModal} onHide={this.close}>
          <Modal.Header closeButton>
            <Modal.Title>{this.props.Info.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.props.Info.body}
            {this.props.Info.body ? null : 
            (
              <div>
                <Input type="text" label="Country" ref="country" placeholder="Enter Country" value={this.state.targetDocument.country} onChange={updateCountry}/>
                <Input type="text" label="Capital" ref="capital" placeholder="Enter Capital" value={this.state.targetDocument.capital} onChange={updateCapital}/>
                <Input type="text" label="Timezone" ref="timezone" bsStyle={this.state.warning} placeholder="Enter Timezone" value={this.state.targetDocument.gmt_diff_hrs} onChange={updateTimezone}/>
              </div>
            )} 
          </Modal.Body>
          <Modal.Footer>
          	<Button onClick={this.callback} bsStyle={this.props.Info.ok_button.bsStyle}>
          		{this.props.Info.ok_button.label}
          	</Button>
            <Button onClick={this.close}>Cancel</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
});
module.exports = CustomModal;
/**
* This component is responsible for edit a document/row of a table
* Upon button press, it will create CustomModal component
*/

import React from "react";
import ReactDOM from "react-dom";
import config from "../../config";
import CustomModal from "./custommodal";
import "isomorphic-fetch";

export default class EditRow extends React.Component {
	constructor(props) {
	    super(props);
		this.state = {};
	}

	createPanel() {
		let title = "Edit & Update Row";
		let ok_button = {};
		ok_button.label = "Update";
		ok_button.bsStyle = "primary";
		let targetDocument = this.props.Document;
		let change = this.props.Change;
		let info = {title, ok_button, targetDocument, change};
		ReactDOM.render(<CustomModal Info={info}/>, document.getElementById('panel'));
	}

	render() {
		const buttonStyle = {
			float:"left"
		};

		return (
			<button type="button" className="close" aria-label="Close" style={buttonStyle} onClick={this.createPanel.bind(this)}>
				<span className="glyphicon glyphicon-edit"></span>
			</button>
		);	
	}
}
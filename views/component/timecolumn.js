/**
*	This component is responsible for displaying time of each capical
*	dependant on momentJS library
*	http://momentjs.com/docs/#/displaying/
*	If existing timezone API fail to retrieve for corresponding captial, warning label will show
*
*	To Do: There is a issue of having a warning message shown on console saying
*	'setState can only update mounted or mounting component'
*	It randomly happens upon delete row. Fix later
*/

import React from "react";
import moment from "moment";

//Conver Client's timezone in hours
var offset_diff = (new Date().getTimezoneOffset()*-1)/60;

export default class TimeColumn extends React.Component {
	constructor(props) {
	    super(props);
	    this.state = {};
	    this.state.Enable = this.props.Enable;

	    if(typeof this.props.Timezone !== "number") {
	    	this.state.Enable = false;
	    	this.state.readableDate = "";
	    } else {
	    	var diff = offset_diff - this.props.Timezone;
		    this.state.readableDate = moment().subtract(diff, "hours").format("D/M/YYYY HH:mm:ss");
	    }
	}

	displayDate() {
		var diff = offset_diff - this.props.Timezone;
		var readableDate = moment().subtract(diff, "hours").format("D/M/YYYY HH:mm:ss");
		this.setState({readableDate:readableDate});	
	}

	componentDidMount(){
		this.displayDate.bind(this);
		if(this.state.Enable) {
			this.state.timer = setInterval(this.displayDate.bind(this), 1000);
		}
	}

	componentWillUnmount(){
		if(this.state.timer) {
			clearInterval(this.state.timer);	
		}
	}

	render() {
		if(!this.state.Enable){
			return (
				<span className="label label-danger">Unable to get Timezone</span>
			);
		} else {
			return (
				<span>{this.state.readableDate}</span>
			);	
		}

		
	}
}

/**
*	This component is responsible for generating weather table
*	dependant on WeatherColumn
*/

import React from "react";
import CustomRow from "./customrow.js";
import Lodash from "lodash";

export default class WeatherTable extends React.Component {
	constructor(props) {
	    super(props);
		this.state = {};
		this.state.data = this.props.data;
		this.dataChange = this.dataChange.bind(this);
	}

	dataChange(targetDocument, option) {
		let find_item =(item) => {
			return item._id == targetDocument._id;
		}
		if(option === "delete") {
			Lodash.remove(this.state.data,find_item);
		}
		if(option === "update") {
			var index = Lodash.findIndex(this.state.data, {_id:targetDocument._id});
			this.state.data[index] = targetDocument;
		}
		if(option === "create") {
			this.state.data.push(targetDocument);
			this.state.data = Lodash.sortBy(this.state.data, ['country']);
		}
		

		this.setState({data: this.state.data});
	}

	componentWillMount(){
		//hack approach, assigning local datachange method to global variable
		window.dataChange = (data, option) => {
			this.dataChange(data, option);   
		};
	}

	render() {
		var rows = [];
		this.state.data.forEach(function(location) {
			rows.push(<CustomRow 	key={location._id}
									Country={location.country} 
									Capital={location.capital} 
									Timezone={location.gmt_diff_hrs} 
									Enable={location.gmt_diff_hrs_exist}
									Id={location._id}
									Change={this.dataChange}/>);
		}.bind(this));

		return (
			<table className="table table-striped table-hover table-responsive">
				<thead>
				  <tr>
				    <th key="Country">Country</th>
				    <th key="Capital">Capital</th>
				    <th key="Weather">Weather</th>
				    <th key="Time">Time</th>
				    <th key="Edit">Edit</th>
				    <th key="Delete">Delete</th>
				  </tr>
				</thead>
				<tbody>
				  {rows}
				</tbody>
			</table>
		)
	}
}


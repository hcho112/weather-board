/**
*	This component is responsible for displaying weather condition of each capital
*	It will fetch weather information from yahoo API
*	Timeout length of fetch is defined in config
*
*	To do: This data return from API contains much more information, may be used to enhance application.
*/

import React from "react";
import 'isomorphic-fetch';
import config from "../../config";

export default class WeatherColumn extends React.Component {
	constructor(props) {
	    super(props);
		this.state = {};
	    this.state.status = "Requesting..";
	}

	componentDidMount(){
		fetch(config.weather_api+'select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="'+this.props.Capital+'")&format=json',{timeout:config.timeout})
			.then(function(result) {
				return result.json();
			}).then(function(data) { 
				var status = "unknown";
				if(data.query.results) {
					status = data.query.results.channel.item.condition.text;
					if(status === "") {
						status = "unknown";
					}
				}
				this.setState({status:status});
			}.bind(this)).catch(function(err) {
				console.log("ERROR: ", err);
			});
	}

	render() {
		if(this.state.status === "unknown"){
			return (
				<div className="label label-danger">Unknown</div>
			);
		} else {
			return (
				<span>{this.state.status}</span>
			);	
		}
	}
}